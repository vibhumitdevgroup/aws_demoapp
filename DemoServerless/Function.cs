using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.SimpleEmail;
using Amazon;
using Amazon.SimpleEmail.Model;
using System.Net.Mail;
using DemoServerless.Models;
using Newtonsoft.Json;
using Amazon.CloudWatchLogs;
using Amazon.EC2;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace DemoServerless
{
    public class Functions
    {
        private Dictionary<string, string> headers = new Dictionary<string, string> {
                    { "Content-Type", "application/json" },
                    { "Access-Control-Allow-Headers", "Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token" },
                    { "Access-Control-Allow-Origin", "*" },
                    { "Access-Control-Allow-Methods", "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT" },
                    { "Access-Control-Allow-Credentials", "true" } };

        public Functions()
        {
        }

        public APIGatewayProxyResponse Get(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.LogLine("Get Request\n");

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "AWS-Response",
                Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
            };

            return response;
        }

        public APIGatewayProxyResponse SendeMail(APIGatewayProxyRequest request, ILambdaContext context)
        {
            string senderAddress = "poornas@vibhumit.com";
            context.Logger.LogLine("eMail Request\n");

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "AWS-Response",
                Headers = headers
            };
            eMailModel log = new eMailModel();
            if (request.Body != null)
                log = JsonConvert.DeserializeObject<eMailModel>(request.Body);
            using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.EUWest1))
            {
                var sendRequest = new SendEmailRequest
                {
                    Source = senderAddress,
                    Destination = new Destination
                    {
                        ToAddresses =
                        new List<string> { log.To }
                    },
                    Message = new Message
                    {
                        Subject = new Content(log.Subject),
                        Body = new Body
                        {
                            Text = new Content
                            {
                                Charset = "UTF-8",
                                Data = log.Body
                            }
                        }
                    },
                };
                try
                {
                    Console.WriteLine("eMail using Amazon SES");
                    log.MessageId = client.SendEmailAsync(sendRequest).Result.ResponseMetadata.RequestId;
                    log.From = senderAddress;
                    context.Logger.LogLine(JsonConvert.SerializeObject(log));
                    Console.WriteLine("The email was sent successfully.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error while sending eMail:", ex);
                }
            }

            return response;
        }

        public APIGatewayProxyResponse SendeMailWithSMTP(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.LogLine("SMTP eMail Request\n");

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "AWS-Response",
                Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
            };

            string from = "poornas@vibhumit.com";
            string fromName = "Poorna Saikam";
            string to = "poornas@vibhumit.com";

            string smtpUserName = "AKIAUCYOTOA227NXCDVL";
            string smtpPassword = "BN+E4hNG/baIzmFvpo0Aeo31uizgWk5csgIYPY0RYJ6f";
            string host = "email-smtp.eu-west-1.amazonaws.com";
            int port = 587;
            string subject = "eMail Subject";

            String body = "eMail Body";

            MailMessage message = new MailMessage
            {
                IsBodyHtml = true,
                From = new MailAddress(from, fromName),
                Subject = subject,
                Body = body
            };
            message.To.Add(new MailAddress(to));

            using (var client = new System.Net.Mail.SmtpClient(host, port))
            {
                client.Credentials = new NetworkCredential(smtpUserName, smtpPassword);
                client.EnableSsl = true;
                try
                {
                    client.Send(message);
                    Console.WriteLine("eMail sent successfully!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error while sending eMail: " + ex.Message);
                }
            }
            return response;
        }

        public APIGatewayProxyResponse GetLogs(APIGatewayProxyRequest request, ILambdaContext context)
        {
            List<eMailModel> resultantList = new List<eMailModel>();
            eMailModel dataReqModel = new eMailModel();
            if (request.Body != null)
                dataReqModel = JsonConvert.DeserializeObject<eMailModel>(request.Body);

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "AWS-Response",
                Headers = headers
            };

            var cwClient = new AmazonCloudWatchLogsClient();
            string filteredPattern = null;

            filteredPattern = "{ $.To = \"*"+ dataReqModel.To +"*\" }";
            var filterSearch = cwClient.FilterLogEventsAsync(new Amazon.CloudWatchLogs.Model.FilterLogEventsRequest() { LogGroupName = "/aws/lambda/Demo-Serverless-Get-W2QD0DPWLAQG", FilterPattern = filteredPattern });

            foreach (var message in filterSearch.Result.Events)
            {
                if (message.Message.IndexOf("\n") != -1)
                {
                    string modelString = message.Message.Substring(message.Message.IndexOf("\n"));
                    var logfromCW = JsonConvert.DeserializeObject<eMailModel>(message.Message);
                    resultantList.Add(logfromCW);
                }
            }
            response.Body = JsonConvert.SerializeObject(resultantList);
            return response;
        }
     }
}
